test -f /etc/bashrc && . /etc/bashrc
test -d ~/.bashrc.d && for rc in ~/.bashrc.d/*
do test -f "$rc" && . "$rc" 2>/dev/null
done
unset rc
