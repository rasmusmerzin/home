# Home Config

Repository for user configuration in the `$HOME` directory.

Cloning/importing this configuration:

```sh
git clone --bare git@gitlab.com:rasmusmerzin/home $HOME/.home
```

Hiding untracked files:

```sh
git --git-dir=$HOME/.home --work-tree=$HOME config --local status.showUntrackedFiles no
```

Restore files:

```sh
git --git-dir=$HOME/.home --work-tree=$HOME restore --staged .
git --git-dir=$HOME/.home --work-tree=$HOME restore .
```
